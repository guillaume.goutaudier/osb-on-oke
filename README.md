# Introduction
The objetive of this tutorial is to demonstrate how to deploy the Oracle Service Broker software (OSB) on the Oracle Kubernetes service (OKE). Most instructions are following this documentation: 
https://oracle.github.io/fmw-kubernetes/soa-domains/

The architecture that will be deployed is the following:
![architecture.png](architecture/architecture.png)

# Prerequisites
- OCI CLI (tested with version 3.0.0)
- Helm (tested with version 3.6.3)
- Docker (tested with version 20.10.7)

# Infrastructure deployment
- We start be deploying the entire OCI infrastructure with Terraform:
```
cd terraform
terraform init
terraform apply
mv output/output.tf .
terraform apply
```
- Use the terraform output to update your kubernetes configuration file
```
export CLUSTER_ID=<from terraform output>
oci ce cluster create-kubeconfig --cluster-id $CLUSTER_ID --file $HOME/.kube/config --region eu-zurich-1 --token-version 2.0.0  --kube-endpoint PUBLIC_ENDPOINT
```

# Environment preparation
- clone the SOA repository
```
git clone https://github.com/oracle/fmw-kubernetes.git --branch release/21.3.2
export WORKDIR=$PWD/fmw-kubernetes/OracleSOASuite/kubernetes
```
- follow the documentation to download the SOA suite image. 
- put the image on a private repository that is accessible from the OKE cluster

# Weblogic operator installation
- install the operator in the `opns` namespace:
```
kubectl create namespace opns
kubectl create serviceaccount -n opns  op-sa
cd $WORKDIR
helm install weblogic-kubernetes-operator charts/weblogic-operator  --namespace opns  --set image=ghcr.io/oracle/weblogic-kubernetes-operator:3.2.1 --set serviceAccount=op-sa --set "domainNamespaces={}" --set "javaLoggingLevel=FINE" --wait
```
- create a `soans` and update the operator configuration to target this namespace:
```
kubectl create namespace soans
helm upgrade --reuse-values --namespace opns --set "domainNamespaces={soans}" --wait weblogic-kubernetes-operator charts/weblogic-operator  
```

# Create persistent volumes
- go to the `create-weblogic-domain-pv-pvc` directory
```
cd ${WORKDIR}/create-weblogic-domain-pv-pvc/
```
- update the `create-pv-pvc-inputs.yaml` file:
- set the `weblogicDomainStorageType` value to `NFS` instead of `HOSTPATH` 
- set the `weblogicDomainStorageNFSServer` value to the IP address of the NFS server
- set the `weblogicDomainStoragePath` to `/osb`
- run the configuration script:
```
./create-pv-pvc.sh -i create-pv-pvc-inputs.yaml -o output_soainfra
```
- create PV and PVC:
```
kubectl create -f output_soainfra/pv-pvcs/soainfra-domain-pv.yaml
kubectl create -f output_soainfra/pv-pvcs/soainfra-domain-pvc.yaml
```
- go back to root directory and mount the volume from a test image:
```
kubectl create -f k8s/busybox.yaml
```
- connect to the buxybox container and update the permissions of the NFS directory:
```
kubectl exec -it busybox -- /bin/sh
chown 777 /u01/oracle/user_projects
```
- edit the `nfs.tf` terraform file, uncomment the export_options and reapply the configuration:
```
terraform apply
```

# Create WebLogic and RCU secrets 
```
cd ${WORKDIR}/create-weblogic-domain-credentials
./create-weblogic-credentials.sh -u weblogic -p Welcome1 -n soans -d soainfra -s soainfra-domain-credentials
cd ${WORKDIR}/create-rcu-credentials
./create-rcu-credentials.sh \
  -u SOA \
  -p Ashie5yeAshie5ye## \
  -a sys \
  -q Ashie5yeAshie5ye## \
  -d soainfra \
  -n soans \
  -s soainfra-rcu-credentials
```

# Create RCU schema
- replace `soasuite:12.2.1.4` the image from your private repository in ${WORKDIR}/create-rcu-schema/common/template/rcu.yaml.template
- create the RCU schema:
```
cd ${WORKDIR}/create-rcu-schema
export DBHOST=<HOST value from pdb_connection terraform output>
export SOA_IMAGE=<image from your private repository>
export SERVICE_NAME=<SERVICE_NAME from pdb_connection terraform output>
./create-rcu-schema.sh \
  -s SOA\
  -t osb \
  -d $DBHOST:1521/$SERVICE_NAME \
  -i $SOA_IMAGE \
  -n soans \
  -q Ashie5yeAshie5ye## \
  -r Ashie5yeAshie5ye## \
  -c SOA_PROFILE_TYPE=SMALL,HEALTHCARE_INTEGRATION=NO
```

# Create SOA Suite Domains
```
cd ${WORKDIR}/create-soa-domain/domain-home-on-pv
```
- edit `create-domain-inputs.yaml` and update the following values:
```
domainType: osb
configuredManagedServerCount: 2
image: $SOA_IMAGE
rcuDatabaseURL: $HOST:1521/$SERVICE_NAME
rcuSchemaPrefix: SOA
```
- generate the k8s manifests with the domain creation script:
```
./create-domain.sh \
  -i create-domain-inputs.yaml \
  -o output
```
- apply the domain manifest:
```
kubectl apply -f output/weblogic-domains/soainfra/domain.yaml
```

# Create a Load Balancer to access the weblogic admin console
- create a load balancer:
```
kubectl create -f k8s/lb.yaml
```
- you should now be able to access the weblogic admin console using the load balancer public URL:
![architecture.png](architecture/weblogic.png)

# To go further
This was just a basic tutorial demonstrating how to install the OSB core components on OKE.
To go further, you may want to:
- deploy a reverse proxy and activate SSL encryption
- scale out the architecture by deploying more kubernetes nodes and more OSB servers


