resource "oci_database_db_system" "db_system" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = var.compartment_ocid
    db_home {
        database {
            admin_password = var.db_system_db_home_database_admin_password
    	    db_name = var.db_system_db_home_database_db_name
        }
        db_version = var.db_system_db_home_db_version
    }
    hostname = var.db_system_hostname
    shape = var.db_system_shape
    ssh_public_keys = [var.ssh_public_keys]
    subnet_id = oci_core_subnet.private-subnet.id
    data_storage_size_in_gb = var.db_system_data_storage_size_in_gb
    database_edition = var.db_system_database_edition
    display_name = "db_system"
    domain = var.db_system_domain
    license_model = var.db_system_license_model
    node_count = "1"
}
