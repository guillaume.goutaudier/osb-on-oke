# ------ Get Ubuntu images
data "oci_core_images" "ubuntu-images" {
  compartment_id = var.compartment_ocid
  operating_system = "Canonical Ubuntu"
  operating_system_version = "20.04"
  sort_by = "TIMECREATED"
  sort_order = "DESC"
}

# ------ Create bastion virtual machine
resource "oci_core_instance" "bastion" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = var.compartment_ocid
    shape = "VM.Standard2.1"
    display_name = "bastion"

    create_vnic_details {
        assign_public_ip = "true"
        subnet_id = oci_core_subnet.public-subnet.id
    }
    metadata = {
        ssh_authorized_keys = var.ssh_public_keys
    }
    source_details {
        source_id = data.oci_core_images.ubuntu-images.images[0].id
        source_type = "image"
    }
}
output "bastion-public-ip" {
  value = oci_core_instance.bastion.public_ip
}

