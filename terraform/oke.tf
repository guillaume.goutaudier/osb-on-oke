# ------ Kubernetes cluster (OKE)
resource "oci_containerengine_cluster" "oke" {
    compartment_id = var.compartment_ocid
    kubernetes_version = "v1.20.8"
    name = "oke"
    vcn_id = oci_core_vcn.vcn.id
    endpoint_config {
        is_public_ip_enabled = "true"
        subnet_id = oci_core_subnet.public-subnet.id
    }
    options {
        service_lb_subnet_ids = [oci_core_subnet.public-subnet.id]
    }
}

# ------ Get latest image
data "oci_core_images" "images" {
  compartment_id = var.compartment_ocid
  operating_system = "Oracle Linux"
  operating_system_version = "7.9"
  shape = "VM.Standard2.4"
  sort_by = "TIMECREATED"
}

# ------ Node pool
resource "oci_containerengine_node_pool" "pool" {
    cluster_id = oci_containerengine_cluster.oke.id
    compartment_id = var.compartment_ocid
    kubernetes_version = "v1.20.8"
    name = "node-pool"
    node_source_details {
        image_id = data.oci_core_images.images.images[0].id
        source_type = "IMAGE"
    }
    node_shape = "VM.Standard2.4"
    node_config_details {
        placement_configs {
            availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
            subnet_id = oci_core_subnet.private-subnet.id
        }
        size = "1"
    }
    ssh_public_key = var.ssh_public_keys
}

# ------ OKE Cluster OCID
output "CLUSTER_ID" {
    value = oci_containerengine_cluster.oke.id
}
