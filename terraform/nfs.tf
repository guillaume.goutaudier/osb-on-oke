# ------ File System in AD1 in the new VCN
resource "oci_file_storage_file_system" "file_system" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = var.compartment_ocid
    display_name = "file_system"
}
resource "oci_file_storage_mount_target" "mount_target" {
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = var.compartment_ocid
    subnet_id = oci_core_subnet.private-subnet.id
    display_name = "mount target"
}
resource "oci_file_storage_export_set" "export_set" {
    mount_target_id = oci_file_storage_mount_target.mount_target.id
}
resource "oci_file_storage_export" "export" {
    export_set_id = oci_file_storage_export_set.export_set.id
    file_system_id = oci_file_storage_file_system.file_system.id
    path = "/osb"
    #export_options {
    #    source = "0.0.0.0/0"
    #    access = "READ_WRITE"
    #    anonymous_gid = "1000"
    #    anonymous_uid = "1000"
    #    identity_squash = "ALL"
    #    require_privileged_source_port = "false"
    #}
}
output "nfs_mount_target" {
    value = oci_file_storage_mount_target.mount_target.ip_address
}
