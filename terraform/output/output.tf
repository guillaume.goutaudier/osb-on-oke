data "oci_database_pluggable_databases" "pluggable_databases" {
    compartment_id = var.compartment_ocid
}
output "pdb_connection" {
    value = data.oci_database_pluggable_databases.pluggable_databases.pluggable_databases[0].connection_strings[0].all_connection_strings["pdbIpDefault"]
}

