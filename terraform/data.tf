# ------ Availability Domains
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_ocid
}

# ------ Public Services CIDR
data "oci_core_services" "paas_services" {
}

